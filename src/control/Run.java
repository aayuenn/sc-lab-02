package control;

import model.BankAccount;
import view.InvestmentFrame;

public class Run {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	   double INITIAL_BALANCE = 1000;
	   BankAccount ba = new BankAccount(INITIAL_BALANCE);
	   InvestmentFrame frame = new InvestmentFrame();
	   Controller c = new Controller(ba, frame);
	}

}
